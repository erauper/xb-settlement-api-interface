// Backend entities:
// - Configuration (corridor, steps, CRUD)
// - Flow (preview, execution)
// - Step: ExchagneStep, VaultStep
// Assumptions:
// - Corridors and their steps should be hard-coded for now

// -------------- Configuration Routes --------------

// Creating a new configuration:
// endpoint: POST /xb-settlements/configs
// body:
interface XBCreateConfigurationRequest {
    corridorId: string; // for now - only 'MX_US'
    name: string; // friendly name
    steps: Record<XBSettlementStepType, XBSettlementConfigStep>
}
// response:
interface XBCreateConfigurationResponse {
    XBSettlementConfigModel
}

// Get a configuration:
// endpoint: GET /xb-settlements/configs/<id>
// body: -
// response:
interface XBGetConfigurationResponse {
    XBSettlementConfigModel
}

// Get all configuration under a tenant:
// endpoint: GET /xb-settlements/configs
// body: - (tenant is part of the jwt)
// response:
export type GetAllXBSettlementConfigsResponse = {
    configurations: Array<XBSettlementConfigModel>;
}

// Updating a configuration
// endpoint: PUT /xb-settlements/configs/<id>
// body:
interface XBUpdateConfigurationRequest {
    name: string; // friendly name
    steps: Record<XBSettlementStepType, XBSettlementConfigStep>
}
// response:
interface XBUpdateConfigurationResponse {
    XBSettlementConfigModel
}

// Deleting a configuration
// endpoint DELETE /xb-settlements/configs/<id>
// response:
interface XBDeleteConfigurationResponse {
    configId: string;
}


// -------------- Flow Routes --------------
// Creating a flow (preview request)
// POST /xb-settlements/flows
// request: 
export interface XBCreateSettlementFlowRequest {
    configId: string;
    amount: XBSettlementAsset; 
}
// response: 
export interface XBCreateSettlementFlowResponse {
    XBGetSettlementFlowSetup
}



// ------------- <DONT IMPLEMENT YET> --------------
// Updating a flow (updated flow preview request) 
// PUT /xb-settlements/flows/<id>  
// ------------- </DONT IMPLEMENT YET> --------------



// Executing a flow (launching a payment)
// POST /xb-settlements/flows/<flow-id>/actions/execute
// request: 
// body: -
// response:
export interface XBSettlementFlowExecutionResponse {
    XBSettlementFlowExecution
}
  
// Getting a flow (in setup (preview) mode, or execution mode)
// GET /xb-settlements/flows/<flow-id>
// request: 
// body: -
// response:
export interface XBGetSettlementFlowExecutionResponse {
    setup?: XBGetSettlementFlowSetup; // will be defined if the flow is in preview mode
    execution?: XBSettlementFlowExecution; // will be defined in the flow is in execution mode
}


// --------------------------------------
// --------------------------------------

export interface XBSettlementConfigModel {
    configId: string;
    tenantId: string;
    corridorId: string;
    name: string; // friendly name
    steps: Record<XBSettlementStepType, XBSettlementConfigStep>
}

export interface XBSettlementConfigStep {
    accountId: string;
}

export enum XBSettlementStepType {
    ON_RAMP = 'ON_RAMP',
    VAULT_ACCOUNT = 'VAULT_ACCOUNT',
    OFF_RAMP = 'OFF_RAMP',
    FIAT_DESTINATION = 'FIAT_DESTINATION',
}
  
export interface XBSettlementFlowSetupStep {
    accountId: string;
    inputAmount: XBSettlementAsset; 
    outputAmount: XBSettlementAsset; 
    estimatedFee: XBSettlementAsset;
    estimatedTime: number; // time in seconds
}


export interface XBGetSettlementFlowSetup {
    flowId: string;
    tenantId: string;
    configId: string;
    conversionRate: string;
    steps: Record<XBSettlementStepType, XBSettlementFlowSetupStep>;
    inputAmount: XBSettlementAsset; 
    estimatedOutputAmount: XBSettlementAsset; 
    totalEstimatedFee: XBSettlementAsset;
    totalEstimatedTime: number; // time in seconds
}

enum XBSettlementFlowExecutionStepStatus {
    NOT_STARTED = 'NOT_STARTED',
    PROCESSING = 'PROCESSING',
    COMPLETED = 'COMPLETED',
    FAILED = 'FAILED',
}

export interface XBSettlementFlowExecutionStep {
    id: string;
    accountId: string;
    status: XBSettlementFlowExecutionStepStatus;
    inputAmount: XBSettlementAsset;
    outputAmount?: XBSettlementAsset;
    fee?: XBSettlementAsset;
    startedAt: number; // milliseconds UTC
    completedAt?: number // milliseconds UTC
}


export enum XBSettlementFlowExecutionStatus {
    PROCESSING = 'PROCESSING',
    COMPLETED = 'COMPLETED',
    FAILED = 'FAILED',
}

export interface XBSettlementFlowExecution {
    flowId: string;
    tenantId: string;
    configId: string; 
    steps: Record<XBSettlementStepType, XBSettlementFlowExecutionStep>;
    inputAmount: XBSettlementAsset; // constant input amount through the process of execution (was set during setup)
    outputAmount: XBSettlementAsset; // output amount (including fees) so far in the process of execution
    totalFee: XBSettlementAsset; // total fee so far in the execution process
    totalTime: number; // total execution time so far in seconds
    initiatedAt: number; // milliseconds UTC. (this time should originate from the execution and not preview)
    initiatedBy: string; // id of the user which launched the flow
    state: XBSettlementFlowExecutionStatus
}

export interface XBSettlementAsset {
    amount: string; 
    assetId: string 
}
